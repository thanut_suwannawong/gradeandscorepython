import csv

def main():
    
    print("##########################################")
    print()
    print("ปีการศึกษา 1/2558 เฉลี่ยต่อเทอม => "+str(calculate_each(1))+"  เฉลี่ยสะสม => "+str(calculate_all(1)))
    print("ปีการศึกษา 2/2558 เฉลี่ยต่อเทอม => "+str(calculate_each(2))+" เฉลี่ยสะสม => "+str(calculate_all(2)))
    print("ปีการศึกษา 1/2559 เฉลี่ยต่อเทอม => "+str(calculate_each(3))+" เฉลี่ยสะสม => "+str(calculate_all(3)))
    print("ปีการศึกษา 2/2559 เฉลี่ยต่อเทอม => "+str(calculate_each(4))+" เฉลี่ยสะสม => "+str(calculate_all(4)))
    print()
    print("##########################################")

def calculate_all(total):
    grade = get_grade()
    credit = get_credit()
    tmp = 0
    all_credit= 0
    i = 0
    while(i<total):
        j=0
        while(j<len(grade[i])):
            all_credit += int(credit[i][j])
            tmp += float(grade[i][j])*int(credit[i][j])
            
            j+=1  
        i+=1
    return round((tmp/all_credit),2)

def calculate_each(term):
    grade = get_grade()
    credit = get_credit()
    tmp = 0
    all_credit= 0
    i=0
    term -= 1
    while(i<len(grade[term])):
         all_credit += int(credit[term][i])
         tmp += float(grade[term][i])*int(credit[term][i])     
         i+=1  
    return round((tmp/all_credit),2)
    

def get_grade():
    term = []
    term_1_grade = []
    term_2_grade = []
    term_3_grade = []
    term_4_grade = []
    
    i = 0
    with open('score.csv', newline='',encoding='utf8') as csvfile:
        grade_record = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in grade_record:
            temp = []
            if(i !=0):
                term.append(', '.join(row))
            i+=1
        j=0
        for row in term:
            if(row[:2] == '1/' or row[:2] == '2/' ):
                j+=1
                if(j==1):
                    term_1_grade.append(row[-3:])
                elif(j==2):
                    term_2_grade.append(row[-3:])
                elif(j==3):
                    term_3_grade.append(row[-3:])
                else:
                    term_4_grade.append(row[-3:])
            else: 
                if(j==1):
                    term_1_grade.append(row[-4:-1])
                elif(j==2):
                    term_2_grade.append(row[-4:-1])
                elif(j==3):
                    term_3_grade.append(row[-4:-1])
                else:
                    term_4_grade.append(row[-4:-1])
                    
        all_grade = [term_1_grade,term_2_grade,term_3_grade,term_4_grade]
        return all_grade
        
def get_credit():
    term = []
    term_1_credit = []
    term_2_credit = []
    term_3_credit = []
    term_4_credit = []
    i = 0
    with open('score.csv', newline='',encoding='utf8') as csvfile:
        grade_record = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in grade_record:
            temp = []
            if(i !=0):
                term.append(', '.join(row))
                
            i+=1
        j=0
        for row in term:
            if(row[:2] == '1/' or row[:2] == '2/' ):
                j+=1
                if(j==1):
                    term_1_credit.append(row[10])
                elif(j==2):
                    term_2_credit.append(row[10])
                elif(j==3):
                    term_3_credit.append(row[10])
                else:
                    term_4_credit.append(row[10])
            else: 
                if(j==1):
                    term_1_credit.append(row[1])
                elif(j==2):
                    term_2_credit.append(row[1])
                elif(j==3):
                    term_3_credit.append(row[1])
                else:
                    term_4_credit.append(row[1])
        all_credit = [term_1_credit,term_2_credit,term_3_credit,term_4_credit]
        return all_credit
    
main()
